import socket, requests, json
hostName = socket.gethostname()
post_data = {'sysId':hostName}
try:
    post_response = requests.post(url="http://sessions/endActiveSessionBySystem.php",data=post_data)
    data = json.loads(post_response.text)
except requests.exceptions.Timeout:
    #Maybe set up for a retry, or continue in a retry loop
    print("Connection Timeout")
    logging.debug('Connection Timeout...')
