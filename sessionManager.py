import tkinter as tk
import socket, requests, json, time, os, datetime, tkinter.messagebox as tkMessageBox, urllib.request as urllib2, logging, webbrowser, getpass, threading

try:
        from configparser import ConfigParser
except ImportError:
        from configparser import ConfigParser

from gi.repository import Notify
#from ScrolledText import ScrolledText
import tkinter.scrolledtext as ScrolledText

devMode = 0
#0 off; 1 on
key = '0'
barcode = None
hostName = socket.gethostname()
locked = 0
expressSession = 0
startMessageDisplayed = 0
patronLockFlag = 0

config = ConfigParser()
config.read('/home/guest/.session_manager/settings.ini')

sessionServer = config.get('session','SessionServer')
prefixes = config.get('session','allowedPrefixes')
prefixes = prefixes.split(',')
expressPrefix = config.get('session','expressPrefix')

messageTime0 = 0
messageTime1 = 0
messageTime2 = 0
messageTime3 = 0
messageTime4 = 0
messageTime5 = 0
messageTime6 = 0
messageTime7 = 0

messageFlag0 = 0
messageFlag1 = 0
messageFlag2 = 0
messageFlag3 = 0
messageFlag4 = 0
messageFlag5 = 0
messageFlag6 = 0
messageFlag7 = 0

#Session Selection Window. First window to be displayed.
class sessionSelect:
    def __init__(self, master):
        #Variables
        self.sessionType = 'none'
        self.barcode = tk.StringVar()
        self.enterPress = 0

        #Elements
        self.master = master
        self.master.title("Session Manager")
        self.master.resizable(width=False, height=False)
        self.master.configure(bg='#535d6c')
        w = self.master.winfo_screenwidth()
        h = self.master.winfo_screenheight()

        if devMode == 1:
            self.frame = tk.Frame(self.master,width=800,height=600)
            self.master.overrideredirect(0)
            self.master.attributes('-fullscreen', False)
        else:
            self.frame = tk.Frame(self.master,width=w,height=h)
            self.master.wm_protocol('WM_DELETE_WINDOW', lambda:doNothing())
            self.master.attributes('-fullscreen', True)

        self.frame.configure(bg='#535d6c')
        self.title = tk.Label(self.master, text="Public Access Computer",bg='#535d6c',fg='#ffffff', font=("Droid bold", 50))
        self.subtitle = tk.Label(self.master, text="Choose a session",bg='#535d6c',fg='#ffffff', font=("Droid bold", 30))
        self.message = tk.Label(self.master, text="",bg='#535d6c',fg='orange', font=("Droid bold", 12),wraplength=600)
        self.barcodeLabel = tk.Label(self.master, text="Enter your barcode to start a session.",bg='#535d6c',fg='#ffffff', font=("Droid bold", 12),wraplength=600)
        self.barcodeEntry = tk.Entry(self.master,textvariable=self.barcode, relief='flat', font=("Droid", 30), justify='center', highlightthickness=0,bd=0,bg='#fff',fg='#000')
        self.barcodeEntry.focus()
        self.fullSessionButton = tk.Button(self.master, text="Start Session", command=self.fullSession, relief='flat',bg='#2199e8', activebackground='#1583cc', activeforeground='#ffffff', fg='#ffffff', font=("Droid", 20), highlightthickness=0,bd=0, pady=15)
        self.expressSessionLabel = tk.Label(self.master, text="Start a 10 minute express session. No barcode required.",bg='#535d6c',fg='#ffffff', font=("Droid bold", 12))
        self.expressSessionButton = tk.Button(self.master, text="Start 10 Minute Express Session", command=self.expressSession, relief='flat',bg='#cccccc', activebackground='#dddddd', activeforeground='#000', fg='#000', font=("Droid", 15), highlightthickness=0,bd=0, pady=30)
        self.note = tk.Label(self.master, text='',fg='#ffffff')

        #Pack Elements
        self.title.pack(pady=(325,10))
        self.message.pack()
        self.barcodeLabel.pack(pady=(10,10))
        self.barcodeEntry.pack(ipadx=10,ipady=10,padx=700, pady=(10,0), fill='x')
        self.fullSessionButton.pack(padx=700, fill='x')

        self.master.bind('<Return>', self.enterBind)
        self.master.bind('<KP_Enter>', self.enterBind)
        self.adminLock()

        if self.checkConnection() == False:
            print("No connection detected")
            logging.debug("No network connection detected.")
            networkAdminLockWindow.deiconify()
            networkAdminLockWindow.update()
            self.master.withdraw()
            self.master.update()
            time.sleep(15)
            logoff()
        else:
            print("Connection established")
            networkAdminLockWindow.withdraw()
            self.endActiveSessions()
            self.frame.pack()
            self.ensureTop()


    def adminLock(self):
        #Elements
        print("Building network lock window.")
        global networkAdminLockWindow
        networkAdminLockWindow = tk.Tk()
        networkAdminLockWindow.configure(bg='#535d6c')
        networkAdminLockWindow.attributes("-alpha",0.99)
        networkAdminLockWindow.title("Admin Lock")
        networkAdminLockWindow.resizable(width=False, height=False)
        networkAdminLockWindow.withdraw()
        if devMode == 1:
            networkAdminLockWindow.overrideredirect(0)
        else:
            networkAdminLockWindow.overrideredirect(1)
        w = networkAdminLockWindow.winfo_screenwidth()
        h = networkAdminLockWindow.winfo_screenheight()
        frame = tk.Frame(networkAdminLockWindow,width=w,height=h)
        frame.configure(bg='#535d6c')
        title = tk.Label(networkAdminLockWindow, text="Connecting...",bg='#535d6c',fg='#ffffff', font=("Droid bold", 30))

        #Pack Elements
        title.pack(fill='x',padx=400,pady=400)
        frame.pack()

    #Brings the Session Select window to the top
    def ensureTop(self):
        try:
            self.master.lift()
            self.master.after(100,self.ensureTop)
        except:
            pass

    def checkConnection(self):
        print("Testing connection...")
        logging.debug("Testing connection...")
        global sessionServer
        try:
            response=urllib2.urlopen('http://%s' % sessionServer,timeout=1)
            logging.debug("Connection established!")
            return True
        except urllib2.URLError as err: pass
        return False

    def endActiveSessions(self):
        global sessionServer
        post_data = {'sysId':hostName}
        try:
            post_response = requests.post(url="http://%s/endActiveSessionBySystem.php" % sessionServer,data=post_data)
            data = json.loads(post_response.text)
        except requests.exceptions.Timeout:
            #Maybe set up for a retry, or continue in a retry loop
            print("Connection Timeout")
            logging.debug('Connection Timeout...')

    def enterBind(self,event):
        if(self.enterPress == 0):
            print('<Enter> key press detected.')
            self.enterPress = 1	
            self.fullSession()
        else:
            print('EnterBind fired but enterPress not detected correctly.')

    def enableExpressButton(self,currentTime):
        unlockTime = currentTime + 10
        if time.time() >= unlockTime:
            self.expressSessionButton.configure(state='normal')
        self.master.after(100, self.enableExpressButton(currentTime))

    #Full session action
    def fullSession(self):
        self.barcodeEntry.configure(state='disabled')
        self.fullSessionButton.configure(state='disabled')
        self.master.bind('<Return>', doNothing)
        self.master.bind('<KP_Enter>', doNothing)
        self.sessionType = 'full'
        global barcode
        barcode = self.barcode.get()
        global hostName
        global sessionServer
        global prefixes
        global expressPrefix

        if barcode != "":
            if barcode.isdigit():
                if len(barcode) == 14:
                    prefix = barcode[0:5]
                    prefixes.append(expressPrefix)
                    if prefix in prefixes:
                        if prefix != expressPrefix:
                            post_data = {'barcode':barcode,'sysId':hostName}
                            post_response = requests.post(url="http://%s/barcodeCheck.php" % sessionServer,data=post_data)
                            print(post_response.text)
                            data = json.loads(post_response.text)
                            status = data['status']
                            note = data['note']
                            self.master.config(cursor="")
                            self.enterPress = 0
                            if status == "0":
                                self.fullSessionButton.configure(state='normal')
                                self.message.configure(text=note)
                                self.barcodeEntry.configure(state='normal')
                                self.barcodeEntry.delete(0,'end')
                            else:
                                print("Barcode checks out. Log into system")
                                self.message.configure(text='Successful login!')
                                self.master.withdraw()
                                self.barcodeEntry.delete(0,'end')
                                root2 = tk.Toplevel()
                                root2.configure(bg='#595e61')
                                root2.attributes("-alpha",0.99)
                                policyWindow = policy(master=root2)
                        else:
                            print("Guest pass detected")
                            #Do guest pass authentication.
                            post_data = {'barcode':barcode}
                            post_response = requests.post(url="http://%s/authExpressSession.php" % sessionServer,data=post_data)
                            data = json.loads(post_response.text)
                            #print(post_response.text)
                            status = data['status']
                            self.enterPress = 0
                            if status == 1:
                                #if passes start a quick session.
                                print("Authetication passed - start quick session")
                                self.message.configure(text='Successful login!')
                                self.master.withdraw()
                                self.barcodeEntry.delete(0,'end')
                                root2 = tk.Toplevel()
                                root2.configure(bg='#595e61')
                                root2.attributes("-alpha",0.99)
                                global expressSession
                                expressSession = 1
                                policyWindow = policy(master=root2)
                            else:
                                #if files disable a message.
                                self.fullSessionButton.configure(state='normal')
                                self.message.configure(text="Invalid Express Session barcode or Express Session restricted. Please see staff for assistance.")
                                self.barcodeEntry.configure(state='normal')
                                self.barcodeEntry.delete(0,'end')				
                    else:
                        print('Invalid consortium card number')
                        self.barcodeEntry.configure(state='normal')
                        self.fullSessionButton.configure(state='normal')
                        self.barcodeEntry.delete(0,'end')
                        self.message.configure(text='Invalid consortium card number')
                        self.enterPress = 0
                else:
                    print('Barcode length incorrect')
                    self.fullSessionButton.configure(state='normal')
                    self.barcodeEntry.configure(state='normal')
                    self.barcodeEntry.delete(0,'end')
                    self.message.configure(text='Barcode length incorrect')
                    self.enterPress = 0
            else:
                print('Barcode not numerical')
                self.fullSessionButton.configure(state='normal')
                self.barcodeEntry.configure(state='normal')
                self.barcodeEntry.delete(0,'end')
                self.message.configure(text='Barcode not numerical')
                self.enterPress = 0
        else:
                print('Barcode required')
                self.fullSessionButton.configure(state='normal')
                self.barcodeEntry.configure(state='normal')
                self.barcodeEntry.delete(0,'end')
                self.message.configure(text='Barcode required')
                self.enterPress = 0

        self.master.bind('<Return>', self.enterBind)
        self.master.bind('<KP_Enter>', self.enterBind)

    #Express Session
    def expressSession(self):
        global expressSession
        expressSession = 1
        self.startExpressSession()
        print("Barcode checks out. Log into system")
        self.message.configure(text='Guest session started!')
        self.master.withdraw()
        self.barcodeEntry.delete(0,'end')
        root2 = tk.Toplevel()
        root2.configure(bg='#595e61')
        root2.attributes("-alpha",0.99)
        policyWindow = policy(master=root2)

    #Start full session
    def startFullSession(self, barcode):
        global hostName
        print("Start full session for "+str(hostName)+" and barcode "+ str(barcode))

    #Start express session
    def startExpressSession(self):
        global hostName
        print("Start express session for "+str(hostName))

#Policy Agreement Window
class policy:
    def __init__(self, master):
        #Elements
        self.master = master
        master.title("Policy")		
        w = self.master.winfo_screenwidth()
        h = self.master.winfo_screenheight()
        master.configure(bg='#333', width=w, height=h)
        master.resizable(width=False, height=False)
        master.geometry("%dx%d+0+0" % (w, h))
	
        if devMode == 1:
            master.overrideredirect(0)
        else:
            master.overrideredirect(1)
            master.wm_protocol('WM_DELETE_WINDOW', lambda:doNothing())

        frame = tk.Frame(master)
        frame.configure(bg='#333')
        frame.columnconfigure(0, weight=2)
        frame.columnconfigure(1, weight=2)
        self.title = tk.Label(frame, text="Policy",bg='#333',fg='#ffffff', font=("Droid bold", 25))
        self.message = tk.Label(frame, text="",bg='#333',fg='#ffffff', font=("Droid bold", 12))
        global expressSession        
        if expressSession == 1:
            self.agreeButton = tk.Button(frame, text="Agree", command=self.agreeExpress, relief='flat',bg='#5fd38d', activebackground='#37c871', activeforeground='#ffffff', fg='#ffffff', font=("Droid", 15), highlightthickness=0,bd=0, pady=20)
        else:
            self.agreeButton = tk.Button(frame, text="Agree", command=self.agree, relief='flat',bg='#5fd38d', activebackground='#37c871', activeforeground='#ffffff', fg='#ffffff', font=("Droid", 15), highlightthickness=0,bd=0, pady=20)
        self.cancelButton = tk.Button(frame, text="Cancel", command=self.cancel, relief='flat',bg='#d35f5f', activebackground='#c83737', activeforeground='#ffffff', fg='#ffffff', font=("Droid", 15), highlightthickness=0,bd=0, pady=20)
        self.agreement = ScrolledText.ScrolledText(frame, wrap='word',state='normal',bg='#ffffff',fg='#000000',font=("Ubuntu Lite",12), relief='flat',highlightthickness=0,bd=0,padx=10,pady=10,width=130,height=30)
        
        #Pack Elements
        frame.grid()
        self.title.grid(row=1, column=0, columnspan=2, sticky=('W','E','N','S'),padx=((w*0.155),0),pady=((h/10),0))
        self.message.grid(row=2, column=0, columnspan=2, sticky=('W','E','N','S'),padx=((w*0.155),0),pady=0)
        self.agreement.grid(row=3, column=0, columnspan=2, sticky=('W','E','N','S'),padx=((w*0.155),0),pady=0)
        self.cancelButton.grid(row=4, column=0, sticky=('W','E','N','S'),padx=((w*0.155),0),pady=0)
        self.agreeButton.grid(row=4, column=1, sticky=('W','E','N','S'),padx=0,pady=0)

        #Functions
        self.getPolicy()

        if self.checkConnection() == False:
            print("No connection detected - logging off system.")
            logoff()

    def agree(self):
        print("Normal Session")
        self.agreeButton.configure(state='disabled')
        self.master.withdraw()
        root3 = tk.Toplevel()
        root3.configure(bg='#595e61')
        root3.attributes("-alpha",0.99)
        sessionManagerWindow = sessionManager(master=root3)

    def agreeExpress(self):
        print("Express Session")
        self.master.withdraw()
        root3 = tk.Toplevel()
        root3.configure(bg='#595e61')
        root3.attributes("-alpha",0.99)
        sessionManagerWindow = sessionManager(master=root3)

    def checkConnection(self):
        global sessionServer
        print("Testing connection")
        try:
            response=urllib2.urlopen('http://%s' % sessionServer,timeout=1)
            return True
        except urllib2.URLError as err: pass
        return False

    def cancel(self):
        logoff()

    def getPolicy(self):
        global sessionServer
        try:
            get_response = requests.post(url="http://%s/eula.php" % sessionServer)
            self.agreement.insert('1.0',get_response.text)
        except:
            self.agreement.insert('1.0','UNABLE TO ACCESS POLICY.')
        self.agreement.configure(state='disabled')
        return

#Session Manager Window
class sessionManager:
    def __init__(self, master):
        #Elements
        self.master = master
        center(master)
        if devMode == 0:
            master.minsize(width=284,height=105)
        else:
            master.minsize(width=280,height=140)

        if expressSession == 1:
            master.minsize(width=284,height=140)

        master.resizable(width=False, height=False)
        master.title("Session Manager")
        #073c5b
        master.configure(bg='#333')        
        if devMode == 0:
            master.wm_protocol('WM_DELETE_WINDOW', lambda:doNothing())
        self.frame = tk.Frame(self.master)
        self.frame.configure(bg='#dedede')
        self.devTitle = tk.Label(master, text="Development Mode",bg='#333',fg='#ffffff', font=("Droid bold", 10))
        self.endTimeLabel = tk.Label(master, text="Session ends at", bg='#333',fg='#ffffff',font=("Droid",12))
        self.endTimeValue = tk.Label(master, text="", bg='#333',fg='#ffffff',font=("Droid",12))
        self.timeLeftLabel = tk.Label(master, text="Time left", bg='#333',fg='#ffffff',font=("Droid",16))
        self.timeLeftValue = tk.Label(master, text="", bg='#333',fg='#ffffff',font=("Droid",16))
        self.message = tk.Label(master, text="",bg='#333',fg='#ffffff', font=("Droid bold", 12),wraplength=600)

        if expressSession == 0:
            self.endButton = tk.Button(master, text="End Session", command=self.endSession, relief='flat',bg='#a02c2c', activebackground='#d40000', activeforeground='#fff', fg='#fff', font=("Droid", 12), highlightthickness=0,bd=0, pady=10,padx=30,width=22)
            self.lockButton = tk.Button(master, text="Lock Computer", command=self.patronLock, relief='flat',bg='#456d84', activebackground='#666', activeforeground='#fff', fg='#fff', font=("Droid", 12), highlightthickness=0,bd=0, pady=10,padx=30,width=22)
        else:
            self.endButton = tk.Button(master, text="End Session", command=self.endSession, relief='flat',bg='#a02c2c', activebackground='#841515', activeforeground='#fff', fg='#fff', font=("Droid", 12), highlightthickness=0,bd=0, pady=10, padx=30, width=22)

        #Grid Elements
        if devMode == 1:
            self.devTitle.grid(row=0,column=0,columnspan=2,padx=5)
        self.message.grid(row=1,column=0, columnspan=2,padx=10)
        self.timeLeftLabel.grid(row=2,column=0,sticky='w',padx=10)
        self.timeLeftValue.grid(row=2,column=1,sticky='w',padx=10)
        self.endTimeLabel.grid(row=3,column=0,sticky='w',padx=10)
        self.endTimeValue.grid(row=3,column=1,sticky='w',padx=10)
        
        if expressSession == 0:
            self.lockButton.grid(sticky='N',row=5,column=0,columnspan=2,padx=(1,1),pady=(0,1))
            self.endButton.grid(sticky='N',row=6,column=0,columnspan=2,padx=(1,1),pady=(0,1))
        else:
            self.endButton.grid(sticky='N',row=5,column=0,columnspan=2,padx=(0,0),pady=(0,1))
        self.frame.grid()
        #Functions to run
        self.buildPatronLockWindow()
        self.adminLock()
        self.register()
        self.setPrintBarcode()

    def setPrintBarcode(self):
        global barcode
        command = "echo \""+ str(barcode) +"\" > ~/.barcode"
        os.system(command)

    def register(self):
        global key
        global expressSession
        global sessionServer
        if expressSession == 1:
            post_data = {'sysId':hostName,'barcode':barcode}
            try:
                post_response = requests.post(url="http://%s/registerquick.php" % sessionServer,data=post_data)
                data = json.loads(post_response.text)
            except requests.exceptions.Timeout:
                # Maybe set up for a retry, or continue in a retry loop
                print("Connection Timeout")
                logging.debug("Connection Timeout")
            except requests.exceptions.TooManyRedirects:
                # Tell the user their URL was bad and try a different one
                print("Too many URL redirects")
                logging.debug("Too many redirects")
            except requests.exceptions.RequestException as e:
                print(e)
                logging.debug(e)
                logoff()
        else:
            post_data = {'sysId':hostName,'barcode':barcode}
            try:
                post_response = requests.post(url="http://%s/register.php" % sessionServer,data=post_data)
                data = json.loads(post_response.text)
                print(data)
            except requests.exceptions.Timeout:
                print("Connection Timeout")
                logging.debug('Connection Timeout...')
            except requests.exceptions.TooManyRedirects:
                print("Too many URL redirects")
                logging.debug('Too many redirects')
            except requests.exceptions.RequestException as e:
                print(e)
                logging.debug(e)
                logoff()
        #print(data)
        key = data['key']
        print("Session registration successful")
        logging.debug('Session Registration successful')
        self.serverTime = int(data['serverTime'])
        self.syncStartTime = self.serverTime-180 #sets sync being time - 3 minutes
        self.syncEndTime = self.serverTime+180 #sets sync end time + 3 minutes
        self.sessionStartTime = int(data['sessionStartTime'])
        self.sessionLength = int(data['sessionLength'])
        timeString = time.strftime('%I:%M %p', time.localtime(int(self.sessionStartTime + self.sessionLength)))
        self.endTimeValue.configure(text=timeString)
        self.updateSession()
        self.evalSession()

    def updateSession(self):
        global adminLockWindow
        global startMessageDisplayed
        global sessionServer
        try:
            post_data = {'sysId':hostName}
            post_response = requests.post(url="http://%s/update2.php" % sessionServer,data=post_data)
            data = json.loads(post_response.text)
            #print(data)
            value = int(data['timeAdjust'])
            status = data['status']
            message = data['message']
            startMessage = data['startMessage']
            currentSessionLength = self.sessionLength
            self.sessionLength = currentSessionLength + int(value)
            timeString = time.strftime('%I:%M %p', time.localtime(int(self.sessionStartTime) + self.sessionLength))
            self.endTimeValue.configure(text=timeString)
            if value != 0:
                v = int(value)
                msg = "Session time adjusted %d seconds" % v
                logging.debug(msg)
            if status == 1:
                #Session is not locked.
                adminLockWindow.withdraw()
            elif status == 0:
                #Session is locked
                adminLockWindow.deiconify()

            startMessageCount = len(startMessage)
            if(startMessageCount > 0 and startMessageDisplayed == 0):
                #tkMessageBox.showinfo("System Message", startMessage)
                sendMessage("System Message",startMessage)
                startMessageDisplayed = 1

            messageCount = len(message)
            if(messageCount > 0):
                sendMessage("System Message",message)        
                #tkMessageBox.showinfo("System Message", message)

        except:
            print("ERROR: Could not communicate with the server...")
            logging.debug("ERROR: Could not communicate with the server...")
            pass

        self.master.after(10000,self.updateSession)

    def evalSession(self):
        i = int(time.time())
        timeRemaining = (self.sessionStartTime + self.sessionLength) - i
        a = datetime.timedelta(seconds=timeRemaining)
        self.timeLeftValue.configure(text=a)
        global messageFlag0
        global messageFlag1
        global messageFlag2
        global messageFlag3
        global messageFlag4
        global messageFlag5
        global messageFlag6
        global messageFlag7
        global patronLockFlag
        if patronLockFlag == 0:
            if timeRemaining in range(601,750): #15 Minute warning
                if messageFlag0 == 0:
                    sendMessage("15 Minute Warning","Less then 15 minutes before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                    messageFlag0 = 1
                elif timeRemaining in range(301,600): #10 Minute warning
                    if messageFlag1 == 0:
                       sendMessage("10 Minute Warning","Less then 10 minutes before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       messageFlag1 = 1
                elif timeRemaining in range(241,300): #5 Minute warning
                    if messageFlag2 == 0:
                       sendMessage("5 Minute Warning","Less then 5 minutes before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       #tkMessageBox.showinfo("5 Minute Warning","Less then 5 minute before session ends. All data will be wiped. Please save your progress or a USB key or email your files to your account.")
                       messageFlag2 = 2
                elif timeRemaining in range(181,240): #4 Minute warning
                    if messageFlag3 == 0:
                       sendMessage("4 Minute Warning","Less then 4 minutes before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       messageFlag3 = 3
                elif timeRemaining in range(121,180): #3 Minute warning
                    if messageFlag4 == 0:
                       sendMessage("3 Minute Warning","Less then 3 minutes before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       messageFlag4 = 1
                elif timeRemaining in range(61,120): #2 Minute warning
                    if messageFlag5 == 0:
                       sendMessage("2 Minute Warning","Less then 2 minutes before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       messageFlag5 = 1
                elif timeRemaining in range(1,60): #1 Minute warning
                    if messageFlag6 == 0:
                       sendMessage("1 Minute Warning","Less then 1 minute before session ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       #tkMessageBox.showwarning("1 Minute Warning","Less then 1 minute before sessione ends. All data will be wiped. Please save your progress to a USB key or email your files to your account.")
                       messageFlag6 = 1
                elif timeRemaining == 0: #0 Minute warning
                    if messageFlag7 == 0:
                       sendMessage("Session Warning","This session has ended.")
                       messageFlag7 = 1
                    endSession()
            elif timeRemaining <= 0:
               print("Time less then 0")
               endSession()

        self.master.after(1000,self.evalSession)

    def endSession(self):
        result = tkMessageBox.askquestion("End Session?", "Are you sure you would like to end your session?", icon='warning')
        if result == 'yes':
            endSession()
        else:
            pass

    #Admin system lock screen
    def adminLock(self):
        #Elements
        global adminLockWindow
        adminLockWindow = tk.Toplevel()
        adminLockWindow.configure(bg='#535d6c')
        adminLockWindow.attributes("-alpha",0.99)
        adminLockWindow.title("Admin Lock")
        adminLockWindow.resizable(width=False, height=False)
        adminLockWindow.withdraw()
        if devMode == 1:
            adminLockWindow.overrideredirect(0)
        else:
            adminLockWindow.overrideredirect(1)
        w = adminLockWindow.winfo_screenwidth()
        h = adminLockWindow.winfo_screenheight()
        frame = tk.Frame(adminLockWindow,width=w,height=h)
        frame.configure(bg='#535d6c')
        title = tk.Label(adminLockWindow, text="System Unavailable :[",bg='#535d6c',fg='#ffffff', font=("Droid bold", 30))

        #Pack Elements
        title.pack(fill='x',padx=400,pady=400)
        frame.pack()

    #Patron system lock screen
    def buildPatronLockWindow(self):
        #Variables
        self.barcodeEntered = tk.StringVar()
        global patronLockWindow

        #Elements
        patronLockWindow = tk.Toplevel()
        patronLockWindow.configure(bg='#595e61')
        patronLockWindow.attributes("-alpha",0.99)
        patronLockWindow.title("Patron Lock")
        patronLockWindow.resizable(width=False, height=False)
        patronLockWindow.withdraw()
        if devMode == 1:
            patronLockWindow.overrideredirect(0)
        else:
            patronLockWindow.overrideredirect(1)
        w = patronLockWindow.winfo_screenwidth()
        h = patronLockWindow.winfo_screenheight()

        self.messageFlag0 = 0
        self.messageFlag1 = 0
        self.messageFlag2 = 0
        self.messageFlag3 = 0
        self.messageFlag4 = 0
        self.messageFlag5 = 0
        self.messageFlag6 = 0
        self.messageFlag7 = 0
        self.remaining = 0

        frame = tk.Frame(patronLockWindow,width=w,height=h)
        frame.configure(bg='#595e61')
        title = tk.Label(patronLockWindow, text="Computer Locked",bg='#595e61',fg='#ffffff', font=("Droid bold", 30))
        self.message = tk.Label(patronLockWindow, text="Use the barcode for this session to unlock the computer.", bg='#595e61', fg='#ffffff', font=("Droid", 12))
        patronLockWindow.barcodeEntry = tk.Entry(patronLockWindow,textvariable=self.barcodeEntered, show='*', relief='flat', font=("Droid", 30), justify='center', highlightthickness=0,bd=0)
        unlockButton = tk.Button(patronLockWindow, text="Unlock", command=self.patronUnlock, relief='flat',bg='#cccccc', activebackground='#dddddd', activeforeground='#000', fg='#000', font=("Droid", 15), highlightthickness=0,bd=0, pady=10,width=10)

        #Pack Elements
        title.pack(padx=500,pady=(300,10))
        self.message.pack(padx=500,pady=(5,5))
        patronLockWindow.barcodeEntry.pack(padx=500,pady=(10,10))
        unlockButton.pack(padx=500,pady=(5,300))
        frame.pack()

        patronLockWindow.bind('<Return>', self.enterBind)
        patronLockWindow.bind('<KP_Enter>', self.enterBind)
        patronLockWindow.after(1, lambda: patronLockWindow.focus_force())

    def enterBind(self,event):
        self.patronUnlock()

    def patronLock(self):
        global patronLockWindow
        global patronLockFlag
        patronLockFlag = 1
        patronLockWindow.deiconify()

    def patronUnlock(self):
        global patronLockWindow
        global barcode
        global patronLockFlag

        if patronLockWindow.barcodeEntry.get() == barcode:
            print("Barcode matches - Unlocking");
            patronLockWindow.barcodeEntry.delete(0,'end');
            patronLockWindow.withdraw();
            patronLockFlag = 0
        else:
            patronLockWindow.barcodeEntry.delete(0,'end');
            print("Barcode does not match!!");
            self.message.configure(text='Incorrect barcode. Please try again.');

#Main application
def main():
    global sessionSelectRoot
    sessionSelectRoot = tk.Tk()
    sessionSelectRoot.configure(bg='#595e61')
    sessionSelectRoot.title('Session Manager')
    sessionSelectWindow = sessionSelect(sessionSelectRoot)
    img = tk.PhotoImage(file='/home/guest/.session_manager/images/session-manager-64x64.png')
    sessionSelectRoot.iconphoto(True, img)
    log()
    sessionSelectRoot.mainloop()

def center(toplevel):
        toplevel.update_idletasks()
        w = toplevel.winfo_screenwidth()
        h = toplevel.winfo_screenheight()
        size = tuple(int(_) for _ in toplevel.geometry().split('+')[0].split('x'))
        x = w/2 - size[0]/2
        y = h/2 - size[1]/2
        toplevel.geometry("%dx%d+%d+%d" % (size + (x, y)))

#Kills entire application
def kill():
    raise SystemExit
    root.destroy()

def logoff():
    #logout = 'xfce4-session-logout --logout'
    logout = 'gnome-session-quit --no-prompt'
    if devMode == 0:
        print("Log off system")
        logging.debug("Log off system")
        os.system(logout)
    else:
        print("Dev Mode: End the session")
    forceClose()

def displayMessage(msg):
    command = "zenity --warning --width=600 --text='<span font=\"14\">%s</span>'" % (msg)
    os.system(command)

def sendMessage(title,message):
    global patronLockFlag
    if patronLockFlag == 0:
        try:
            t = threading.Thread(target=displayMessage, kwargs=({'msg':message}))
            t.start()
        except:
            print("Error: System message couldn't be sent.")
            logging.debug("Error: System message couldn't be sent.")
            pass

def forceClose():
    logging.debug("Force closed executed")
    kill()

def endSession():
    global key
    global sessionServer
    post_data = {'sysId':hostName,'key':key}
    post_response = requests.post(url="http://%s/end.php" % sessionServer,data=post_data)
    data = json.loads(post_response.text)
    if data['status'] == "1":
        print("Session ended successfully.")
        print("Initiating system logoff.")
        logging.debug("Session ended successfully")
        logoff()
    else:
        print("Session unable to end successfully.")
        print("Initiating system logoff.")
        logging.debug("Session unable to end successfully")
        logoff()

def doNothing():
        logging.debug("Application close prevented")
        pass

def log():
    dateTag = datetime.datetime.now().strftime("%Y-%b-%d_%H-%M-%S")
    logging.basicConfig(filename="/tmp/sessionManager_%s.log" % dateTag, level=logging.DEBUG)

if __name__ == '__main__':
    main()
